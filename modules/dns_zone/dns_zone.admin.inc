<?php
/**
 * @file
 * Admin forms and callbacks for DNS zone module.
 */

/**
 * General settings form for the zone module.
 */
function dns_zone_settings_form($form, &$form_state) {
  $form['dns_zone_tld'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow TLD zones'),
    '#default_value' => variable_get('dns_zone_tld', 0),
  );

  return system_settings_form($form);
}

/**
 * DNS zone form for creating and editing DNS zone entities.
 *
 */
function dns_zone_form($form, &$form_state, $zone = NULL) {
  if (empty($zone)) {
    $zone = entity_create('dns_zone', array());
  }

  if (empty($zone->name)) {
    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#description' => t('The zone name.'),
      '#default_value' => $zone->name,
      '#required' => TRUE,
    );
  }

  // Add the field related form elements.
  field_attach_form('dns_zone', $zone, $form, $form_state);

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
  );

  if (!empty($zone->zone_id)) {
    $form['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#weight' => 101,
      '#submit' => array('dns_zone_form_submit_delete'),
      '#limit_validation_errors' => array(),
    );
  }

  $form_state['zone'] = $zone;

  return $form;
}

/**
 * Validate handler for dns_zone_form().
 */
function dns_zone_form_validate(&$form, &$form_state) {
  // Handle field validation.
  field_attach_validate('dns_zone', $form_state['zone'], $form, $form_state);
}

/**
 * Submit handler for dns_zone_form().
 */
function dns_zone_form_submit(&$form, &$form_state) {
  $zone = $form_state['zone'];
  // Add the field info to the zone entity.
  field_attach_submit('dns_zone', $zone, $form, $form_state);
  if (empty($zone->name)) {
    $zone->name = $form_state['values']['name'];
  }
  $zone->save();

  // Confirm creation and redirect to the new zone page.
  drupal_set_message(t('New zone %zone created', array('%zone' => $zone->name)));
  $form_state['redirect'] = 'dns/zone/' . $zone->name;
}

/**
 * Submit handler for the zone delete button.
 */
function dns_zone_form_submit_delete($form, &$form_state) {
  $zone = $form_state['zone'];
  $form_state['redirect'] = 'dns/zone/' . $zone->name . '/delete/form';
}

/**
 * Corfirmation form for deleting dns zone entities.
 */
function dns_zone_delete_confirm_form($form, &$form_state, $zone, $origin = 'overview') {
  $form_state['zone'] = $zone;
  $question = t('Are you sure you want to delete the DNS zone %zone?', array('%zone' => $zone->label()));
  $description = t('This action cannot be undone.');
  $path = 'dns/zone/';
  if ($origin == 'form') {
    $path .= $zone->name .'/edit/';
  }
  $yes = t('Delete');
  $no = t('Cancel');
  return confirm_form($form, $question, $path, "<p>$question</p><p>$description</p>", $yes, $no, 'confirm');
}

/**
 * Submit handler for dns_zone_delete_confirm_form().
 */
function dns_zone_delete_confirm_form_submit(&$form, &$form_state) {
  $form_state['zone']->delete();
  $form_state['redirect'] = 'dns/zone';
}

/**
 * Page callback, view the DNS Zone.
 *
 * @param $zone.
 *  The loaded dns zone.
 */
function dns_zone_view_page($zone) {
  return $zone->view();
}
