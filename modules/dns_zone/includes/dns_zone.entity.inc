<?php
/**
 * @file
 * DNSZone class.
 */

class DNSZone extends Entity {
  /**
   * Returns the "pretty" formatted name for the domain.
   */
  protected function defaultLabel() {
    return $this->name;
  }

  /**
   * Returns the URL to the standard view of this entity.
   */
  protected function defaultUri() {
    return array('path' => 'dns/zone/' . $this->name);
  }
}
