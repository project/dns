<?php
/**
 * @file
 * Entity controller class for the DNS Zone module.
 */

class DNSZoneController extends EntityAPIController {
  /**
   * Called when entity is created.
   */
  public function create(array $values = array()) {
    global $user;

    // Add some default values.
    $values += array(
      'name' => '',
    );

    // Enforce sane values for created/changed timestamps.
    $values['created'] = gmdate('Y-m-d\Th:i:s');
    $values['changed'] = NULL;

    return parent::create($values);
  }


  /**
   * Called when the entity is saved.
   */
  public function save($zone, DatabaseTransaction $transaction = NULL) {
    // If owner information was not provided, default to the current user.
    if (!isset($zone->owner_entity_type) || !isset($zone->owner_entity_id)) {
      global $user;
      $zone->owner_entity_id = $user->uid;
      $zone->owner_entity_type = 'user';
    }
    return parent::save($zone, $transaction);
  }
}
