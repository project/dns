<?php
/**
 * @file
 * Admin forms and callbacks for DNS record module.
 */

/**
 * Entity settings form and placeholder needed by Drupal.
 */
function dns_record_settings_form($form, &$form_state) {
  return $form;
}

/**
 * DNS record form for creating and editing DNS record entities.
 */
function dns_record_form($form, &$form_state, $record = NULL, $zone = NULL) {
  global $user;
  if (empty($record)) {
    $record = entity_create('dns_record', array('owner_entity_type' => 'dns_zone'));
  }

  if ($zone) {
    $form['zone_name'] = array(
      '#type' => 'value',
      '#value' => $zone->name,
    );
  }
  elseif (empty($record->zone_name)) {
    $zone_options = array();
    $zones = dns_zone_load_multiple(FALSE, array('user_id' => $user->uid));
    foreach ($zones as $zone) {
      $zone_options[$zone->name] = $zone->label();
    }
    $form['zone_name'] = array(
      '#type' => 'select',
      '#title' => t('DNS Zone'),
      '#description' => t('The DNS zone this record belongs to.'),
      '#options' => $zone_options,
      '#default_value' => $record->zone_name,
      '#required' => TRUE,
    );
  }


  $form['prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('Prefix'),
    '#description' => t('Prefix for the zone name, eg. “test” for the domain name “test.example.com” in the zone “example.com”.'),
    '#default_value' => $record->prefix,
  );

  $form['record_type'] = array(
    '#type' => 'select',
    '#title' => t('Record type'),
    '#description' => t('The type of DNS record.'),
    '#options' => dns_record_type_list(),
    '#default_value' => $record->record_type,
    '#required' => TRUE,
  );

  $form['ttl'] = array(
    '#type' => 'textfield',
    '#title' => t('TTL'),
    '#description' => t('Time to live'),
    '#default_value' => $record->ttl,
    '#required' => TRUE,
  );

  $form['address'] = array(
    '#type' => 'textfield',
    '#title' => t('IP address'),
    '#default_value' => $record->address,
    '#states' => array(
      'visible' => array(
        ':input[name="record_type"]' => array(array('value' => "A"), array('value' => "AAAA")),
      ),
    ),
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('The DNS name this record points at, for CNAME, MX and similar records'),
    '#default_value' => $record->name,
    '#states' => array(
      'visible' => array(
        ':input[name="record_type"]' => array(array('value' => "CNAME"), array('value' => "MX"), array('value' => "SRV")),
      ),
    ),
  );

  $form['port'] = array(
    '#type' => 'textfield',
    '#title' => t('Port number'),
    '#default_value' => $record->port,
    '#states' => array(
      'visible' => array(
        ':input[name="record_type"]' => array(array('value' => "SRV")),
      ),
    ),
  );

  $form['priority'] = array(
    '#type' => 'textfield',
    '#title' => t('Priority'),
    '#description' => t('Detirmines which record should take precedence in case of matching records'),
    '#default_value' => $record->priority,
    '#states' => array(
      'visible' => array(
        ':input[name="record_type"]' => array(array('value' => "MX"), array('value' => "SRV")),
      ),
    ),
  );

  $form['weight'] = array(
    '#type' => 'textfield',
    '#title' => t('Weight'),
    '#default_value' => $record->weight,
    '#states' => array(
      'visible' => array(
        ':input[name="record_type"]' => array(array('value' => "SRV")),
      ),
    ),
  );

  $form['text_content'] = array(
    '#type' => 'textarea',
    '#title' => t('Text content'),
    '#description' => t('Text content for records that need this, like TXT, SPF, SRV, etc.'),
    '#default_value' => $record->text_content,
    '#states' => array(
      'visible' => array(
        ':input[name="record_type"]' => array(array('value' => "TXT")),
      ),
    ),
  );

  // Add the field related form elements.
  field_attach_form('dns_record', $record, $form, $form_state);

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form_state['record'] = $record;

  return $form;
}

/**
 * Validate handler for dns_record_form().
 */
function dns_record_form_validate(&$form, &$form_state) {
  // Handle field validation.
  field_attach_validate('dns_record', $form_state['record'], $form, $form_state);

  foreach ($form_state['values'] as $name => &$value) {
    $value = trim($value);
    $value = trim($value, '.');
  }

  // Shortcut for shorter if statements below.
  $v = &$form_state['values'];

  // Validate prefix.
  if ($v['prefix'] != '' && $v['prefix'] != '@' && !preg_match('/^[_a-z\d*](?:[-\.]?[_a-z\d]+)*$/', $v['prefix'])) {
    form_set_error('prefix', t('Illegal prefix detected'));
  }

  if (!is_numeric($v['ttl'])) {
    form_set_error('ttl', t('TTL must be an integer larger than 0.'));
  }

  if ($v['address'] && !preg_match('/^\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b$/', $v['address'])) {
    form_set_error('address', t('Invalid IP address.'));
  }

  foreach (explode('.', $v['address']) as $ip) {
    if ($ip > 255) {
      form_set_error('address', t('Invalid IP address'));
    }
  }
  if ($v['priority'] && !is_numeric($v['priority'])) {
    form_set_error('priority', t('Priority must be a whole number.'));
  }
}

/**
 * Submit handler for dns_record_form().
 */
function dns_record_form_submit(&$form, &$form_state) {
  $record = $form_state['record'];
  // Add the field info to the record entity.
  field_attach_submit('dns_record', $record, $form, $form_state);
  $v = $form_state['values'];

  if (!empty($v['zone_name'])) {
    $record->zone_name = $v['zone_name'];
  }

  $record->record_type = $v['record_type'];

  // These fields are optional, so only set them on the record if they
  // actually have a value.
  foreach (array('address', 'name', 'prefix', 'priority', 'text_content', 'ttl', 'weight') as $fieldname) {
    if (!empty($v[$fieldname]) || $v[$fieldname] === '0') {
      $record->$fieldname = $v[$fieldname];
    }
    else {
      $record->$fieldname = NULL;
    }
  }

  if ($record->owner_entity_type == 'dns_zone' && empty($record->owner_entity_id)) {
    $zone = dns_zone_name_load($record->zone_name);
    $record->owner_entity_id = $zone->zone_id;
  }

  if (!empty($record->is_new)) {
    drupal_set_message(t('New record of type %type created.', array('%type' => $record->record_type)));
  }
  else {
    drupal_set_message(t('Record of type %type updated.', array('%type' => $record->record_type)));
  }

  $record->save();

  if (!empty($record->zone_name)) {
    $form_state['redirect'] = 'dns/zone/' . $record->zone_name;
  }
}

/**
 * Corfirmation form for deleting dns record entities.
 */
function dns_record_delete_confirm_form($form, &$form_state, $dns_record) {
  $form_state['dns_record'] = $dns_record;
  $question = t('Are you sure you want to delete the DNS record %record?', array('%record' => $dns_record->label()));
  $description = t('This action cannot be undone.');
  $path = 'dns/record';
  $yes = t('Delete');
  $no = t('Cancel');
  return confirm_form($form, $question, $path, "<p>$question</p><p>$description</p>", $yes, $no, 'confirm');
}

/**
 * Submit handler for dns_record_delete_confirm_form().
 */
function dns_record_delete_confirm_form_submit(&$form, &$form_state) {
  $form_state['dns_record']->delete();
  $form_state['redirect'] = 'dns/record';
}

