<?php

/**
 * Implements hook_views_default_views().
 */
function dns_record_views_default_views() {
  $views = array();

  $view = new view;
  $view->name = 'dns_record';
  $view->description = '';
  $view->tag = 'dns_record';
  $view->base_table = 'dns_records';
  $view->human_name = 'DNS record list';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Records';
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '25';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'record_id' => 'record_id',
    'record_type' => 'record_type',
    'address' => 'address',
    'nothing_1' => 'nothing_1',
    'nothing' => 'nothing_1',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'record_id' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'record_type' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'address' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'nothing_1' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'nothing' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['override'] = 1;
  $handler->display->display_options['style_options']['sticky'] = 0;
  $handler->display->display_options['style_options']['empty_table'] = 0;
  /* Field: DNS record: Dns record ID */
  $handler->display->display_options['fields']['record_id']['id'] = 'record_id';
  $handler->display->display_options['fields']['record_id']['table'] = 'dns_records';
  $handler->display->display_options['fields']['record_id']['field'] = 'record_id';
  $handler->display->display_options['fields']['record_id']['label'] = '';
  $handler->display->display_options['fields']['record_id']['exclude'] = TRUE;
  $handler->display->display_options['fields']['record_id']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['record_id']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['record_id']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['record_id']['alter']['external'] = 0;
  $handler->display->display_options['fields']['record_id']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['record_id']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['record_id']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['record_id']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['record_id']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['record_id']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['record_id']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['record_id']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['record_id']['alter']['html'] = 0;
  $handler->display->display_options['fields']['record_id']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['record_id']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['record_id']['hide_empty'] = 0;
  $handler->display->display_options['fields']['record_id']['empty_zero'] = 0;
  $handler->display->display_options['fields']['record_id']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['record_id']['separator'] = '';
  $handler->display->display_options['fields']['record_id']['format_plural'] = 0;
  /* Field: DNS record: Prefix */
  $handler->display->display_options['fields']['prefix']['id'] = 'prefix';
  $handler->display->display_options['fields']['prefix']['table'] = 'dns_records';
  $handler->display->display_options['fields']['prefix']['field'] = 'prefix';
  $handler->display->display_options['fields']['prefix']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['prefix']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['prefix']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['prefix']['alter']['external'] = 0;
  $handler->display->display_options['fields']['prefix']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['prefix']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['prefix']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['prefix']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['prefix']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['prefix']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['prefix']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['prefix']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['prefix']['alter']['html'] = 0;
  $handler->display->display_options['fields']['prefix']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['prefix']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['prefix']['hide_empty'] = 0;
  $handler->display->display_options['fields']['prefix']['empty_zero'] = 0;
  $handler->display->display_options['fields']['prefix']['hide_alter_empty'] = 1;
  /* Field: DNS record: Zone_name */
  $handler->display->display_options['fields']['zone_name']['id'] = 'zone_name';
  $handler->display->display_options['fields']['zone_name']['table'] = 'dns_records';
  $handler->display->display_options['fields']['zone_name']['field'] = 'zone_name';
  $handler->display->display_options['fields']['zone_name']['exclude'] = TRUE;
  $handler->display->display_options['fields']['zone_name']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['zone_name']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['zone_name']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['zone_name']['alter']['external'] = 0;
  $handler->display->display_options['fields']['zone_name']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['zone_name']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['zone_name']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['zone_name']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['zone_name']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['zone_name']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['zone_name']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['zone_name']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['zone_name']['alter']['html'] = 0;
  $handler->display->display_options['fields']['zone_name']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['zone_name']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['zone_name']['hide_empty'] = 0;
  $handler->display->display_options['fields']['zone_name']['empty_zero'] = 0;
  $handler->display->display_options['fields']['zone_name']['hide_alter_empty'] = 1;
  /* Field: DNS record: Record_type */
  $handler->display->display_options['fields']['record_type']['id'] = 'record_type';
  $handler->display->display_options['fields']['record_type']['table'] = 'dns_records';
  $handler->display->display_options['fields']['record_type']['field'] = 'record_type';
  $handler->display->display_options['fields']['record_type']['label'] = 'Record type';
  $handler->display->display_options['fields']['record_type']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['record_type']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['record_type']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['record_type']['alter']['external'] = 0;
  $handler->display->display_options['fields']['record_type']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['record_type']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['record_type']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['record_type']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['record_type']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['record_type']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['record_type']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['record_type']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['record_type']['alter']['html'] = 0;
  $handler->display->display_options['fields']['record_type']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['record_type']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['record_type']['hide_empty'] = 0;
  $handler->display->display_options['fields']['record_type']['empty_zero'] = 0;
  $handler->display->display_options['fields']['record_type']['hide_alter_empty'] = 1;
  /* Field: DNS record: Address */
  $handler->display->display_options['fields']['address']['id'] = 'address';
  $handler->display->display_options['fields']['address']['table'] = 'dns_records';
  $handler->display->display_options['fields']['address']['field'] = 'address';
  $handler->display->display_options['fields']['address']['label'] = 'IP address';
  $handler->display->display_options['fields']['address']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['address']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['address']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['address']['alter']['external'] = 0;
  $handler->display->display_options['fields']['address']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['address']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['address']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['address']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['address']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['address']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['address']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['address']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['address']['alter']['html'] = 0;
  $handler->display->display_options['fields']['address']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['address']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['address']['hide_empty'] = 0;
  $handler->display->display_options['fields']['address']['empty_zero'] = 0;
  $handler->display->display_options['fields']['address']['hide_alter_empty'] = 1;
  /* Field: DNS record: Ttl */
  $handler->display->display_options['fields']['ttl']['id'] = 'ttl';
  $handler->display->display_options['fields']['ttl']['table'] = 'dns_records';
  $handler->display->display_options['fields']['ttl']['field'] = 'ttl';
  $handler->display->display_options['fields']['ttl']['label'] = 'TTL';
  $handler->display->display_options['fields']['ttl']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['ttl']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['ttl']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['ttl']['alter']['external'] = 0;
  $handler->display->display_options['fields']['ttl']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['ttl']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['ttl']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['ttl']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['ttl']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['ttl']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['ttl']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['ttl']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['ttl']['alter']['html'] = 0;
  $handler->display->display_options['fields']['ttl']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['ttl']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['ttl']['hide_empty'] = 0;
  $handler->display->display_options['fields']['ttl']['empty_zero'] = 0;
  $handler->display->display_options['fields']['ttl']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['ttl']['separator'] = '';
  $handler->display->display_options['fields']['ttl']['format_plural'] = 0;
  /* Field: Edit link */
  $handler->display->display_options['fields']['nothing_1']['id'] = 'nothing_1';
  $handler->display->display_options['fields']['nothing_1']['table'] = 'views';
  $handler->display->display_options['fields']['nothing_1']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing_1']['ui_name'] = 'Edit link';
  $handler->display->display_options['fields']['nothing_1']['label'] = 'Action';
  $handler->display->display_options['fields']['nothing_1']['alter']['text'] = 'Edit';
  $handler->display->display_options['fields']['nothing_1']['alter']['make_link'] = 1;
  $handler->display->display_options['fields']['nothing_1']['alter']['path'] = 'dns/record/[record_id]/edit';
  $handler->display->display_options['fields']['nothing_1']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['nothing_1']['alter']['external'] = 0;
  $handler->display->display_options['fields']['nothing_1']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['nothing_1']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['nothing_1']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['nothing_1']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['nothing_1']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['nothing_1']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['nothing_1']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['nothing_1']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['nothing_1']['alter']['html'] = 0;
  $handler->display->display_options['fields']['nothing_1']['element_type'] = 'div';
  $handler->display->display_options['fields']['nothing_1']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['nothing_1']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['nothing_1']['hide_empty'] = 0;
  $handler->display->display_options['fields']['nothing_1']['empty_zero'] = 0;
  $handler->display->display_options['fields']['nothing_1']['hide_alter_empty'] = 0;
  /* Field: Delete link */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['ui_name'] = 'Delete link';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = 'Delete';
  $handler->display->display_options['fields']['nothing']['alter']['make_link'] = 1;
  $handler->display->display_options['fields']['nothing']['alter']['path'] = 'dns/record/[record_id]/delete';
  $handler->display->display_options['fields']['nothing']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['external'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['nothing']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['nothing']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['html'] = 0;
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['nothing']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['nothing']['hide_empty'] = 0;
  $handler->display->display_options['fields']['nothing']['empty_zero'] = 0;
  $handler->display->display_options['fields']['nothing']['hide_alter_empty'] = 0;
  /* Sort criterion: DNS record: Record_type */
  $handler->display->display_options['sorts']['record_type']['id'] = 'record_type';
  $handler->display->display_options['sorts']['record_type']['table'] = 'dns_records';
  $handler->display->display_options['sorts']['record_type']['field'] = 'record_type';
  /* Sort criterion: DNS record: Prefix */
  $handler->display->display_options['sorts']['prefix']['id'] = 'prefix';
  $handler->display->display_options['sorts']['prefix']['table'] = 'dns_records';
  $handler->display->display_options['sorts']['prefix']['field'] = 'prefix';
  /* Sort criterion: DNS record: Priority */
  $handler->display->display_options['sorts']['priority']['id'] = 'priority';
  $handler->display->display_options['sorts']['priority']['table'] = 'dns_records';
  $handler->display->display_options['sorts']['priority']['field'] = 'priority';
  /* Contextual filter: DNS record: Zone_name */
  $handler->display->display_options['arguments']['zone_name']['id'] = 'zone_name';
  $handler->display->display_options['arguments']['zone_name']['table'] = 'dns_records';
  $handler->display->display_options['arguments']['zone_name']['field'] = 'zone_name';
  $handler->display->display_options['arguments']['zone_name']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['zone_name']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['zone_name']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['zone_name']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['zone_name']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['zone_name']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['zone_name']['glossary'] = 0;
  $handler->display->display_options['arguments']['zone_name']['limit'] = '0';
  $handler->display->display_options['arguments']['zone_name']['transform_dash'] = 0;
  $handler->display->display_options['arguments']['zone_name']['break_phrase'] = 0;
  $translatables['dns_record'] = array(
    t('Master'),
    t('Records'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('.'),
    t('Prefix'),
    t('Zone_name'),
    t('Record type'),
    t('IP address'),
    t('TTL'),
    t('Action'),
    t('Edit'),
    t('dns/record/[record_id]/edit'),
    t('Delete'),
    t('dns/record/[record_id]/delete'),
    t('All'),
  );

  $views[$view->name] = $view;

  return $views;
}
