<?php
/**
 * @file
 * DNSRecord class.
 */

class DNSRecord extends Entity {
  /**
   * Returns the "pretty" formatted name for the domain.
   */
  protected function defaultLabel() {
    $record_type = implode('.', array_filter(array(
      $this->prefix,
      $this->name,
    ))) . ' ' . $this->record_type;
    return t('@record_type record for zone @zone', array('@record_type' => $record_type, '@zone' => $this->zone_name));
  }

  /**
   * Returns the URL to the standard view of this entity.
   */
  protected function defaultUri() {
    return array('path' => 'dns/record/' . $this->name);
  }
}
