<?php
/**
 * @file
 * Entity controller class for the DNS Record module.
 */

class DNSRecordController extends EntityAPIController {
  /**
   * Called when entity is created.
   */
  public function create(array $values = array()) {
    global $user;

    // Add some default values.
    $values += array(
      'name' => NULL,
      'user_id' => $user->uid,
      'zone_name' => NULL,
      'prefix' => NULL,
      'record_type' => 'A',
      'ttl' => 43200,
      'address' => NULL,
      'port' => NULL,
      'priority' => 10,
      'text_content' => NULL,
      'weight' => 10,
    );

    // Enforce sane values for created/changed timestamps.
    $values['created'] = gmdate('Y-m-d\Th:i:s');
    $values['changed'] = NULL;

    return parent::create($values);
  }

  /**
   * Called when the entity is saved.
   */
  public function save($record, DatabaseTransaction $transaction = NULL) {
    // Need to handle NULL values for zone_name seperately as Drupal will use
    // an empty string instead.
    if ($record->zone_name === NULL) {
      unset($record->zone_name);
      if (!empty($record->record_id)) {
        db_update('dns_records')
          ->fields(array('zone_name' => NULL))
          ->condition('record_id', $record->record_id)
          ->execute();
      }
    }
    return parent::save($record, $transaction);
  }
}
