<?php
/**
 * @file
 * Public menu callbacks for the dns module.
 */

/**
 * The front page for the dns administation.
 */
function dns_front_page() {
  // For now we just need something on the page.
  // TODO: Create some kind of dashboard.
  return t('Dashboard.');
}
